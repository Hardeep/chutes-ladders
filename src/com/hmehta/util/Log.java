package com.hmehta.util;

import com.hmehta.ChutesAndLadders;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Log4j (version 2) Logger
 * 
 * @author HMehta
 */
public class Log {

    private final static Logger logger = LogManager.getLogger(ChutesAndLadders.class.getName());

    /**
     * Returns the Logger instance
     * 
     * @return Logger
     */
    public static Logger getLogger() {
        return logger;
    }

}
