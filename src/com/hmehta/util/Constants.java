package com.hmehta.util;

/**
 *
 * @author HMehta
 */
public class Constants {

    public static final String GAME_MODE_EASY = "EASY";
    public static final String GAME_MODE_HARD = "HARD";
    public static final String GAME_MODE_DEFAULT = "DEFAULT";
    // default mode
    public static final int[] DEFAULT_SPECIAL_SQUARE_LOCATION = {0, 3, 8, 20, 27, 35, 50, 70, 79, 15, 46, 48, 55, 61, 63, 86, 92, 94, 97};
    public static final int[] DEFAULT_SPECIAL_SQUARE_GOTO_LOCATION = {38, 14, 31, 42, 84, 44, 68, 91, 100, 6, 26, 11, 53, 19, 60, 24, 73, 75, 78};
    // easy mode
    public static final int[] EASY_SPECIAL_SQUARE_LOCATION = {0, 3, 8, 20, 27, 35, 50, 70, 79, 5, 19, 59, 72, 23, 10};
    public static final int[] EASY_SPECIAL_SQUARE_GOTO_LOCATION = {38, 14, 31, 42, 84, 44, 68, 91, 100, 17, 62, 64, 93, 87, 49};
    // hard mode
    public static final int[] HARD_SPECIAL_SQUARE_LOCATION = {15, 46, 48, 55, 63, 86, 92, 94, 97, 83, 90, 41, 67, 43, 37};
    public static final int[] HARD_SPECIAL_SQUARE_GOTO_LOCATION = {6, 26, 11, 53, 60, 24, 73, 75, 78, 28, 71, 21, 51, 36, 1};

}
