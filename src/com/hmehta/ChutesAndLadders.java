package com.hmehta;

import java.util.List;
import java.util.Arrays;
import com.hmehta.util.Log;
import java.util.ArrayList;
import com.hmehta.model.Square;
import com.hmehta.model.Player;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.hmehta.model.Gameboard;
import java.util.Collections;
import java.util.TreeMap;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.CommandLineParser;

/**
 * Simulates the Chutes and Ladders game in console.
 *
 * @author HMehta
 */
public class ChutesAndLadders {

    private static String gameMode = null;
    private static Gameboard gameboard = null;
    private static List<Player> players = new ArrayList();

    /**
     * main method of the application
     *
     * @param args
     */
    public static void main(String[] args) {
        if (parseCommandLineOptions(args) && (players != null && players.size() >= 2 && players.size() <= 4)) {
            // setup new Gameboard
            gameboard = new Gameboard(gameMode)
                    
                    
                    
                    ;
            if (gameboard != null) {
                decideTurns();
                play();
            }
        }
    }

    /**
     * Each player spins to decide their turn (Higher spin count goes first)
     */
    private static void decideTurns() {
        TreeMap tm = new TreeMap(Collections.reverseOrder());
        Log.getLogger().info("Deciding player turns...");
        for (Player player : players) {
            int score = 0;
            while (score == 0 || tm.containsKey(score)) {
                score = player.spin();
            }
            tm.put(score, player);
            Log.getLogger().info(player.getName() + " spun " + score);
        }
        players = new ArrayList(tm.values());
        Log.getLogger().info("Turn order is as follows: " + players);
    }

    /**
     * Beings the board game play.asdasdasdasdasdasdadsadasdasd
     */
    private static void play() {
        boolean gameWon = false;
        Log.getLogger().info("Starting game play...");
        while (!gameWon) {
            for (Player player : players) {
                player = processTurn(player);
                if (player.getCurrentLocation() == 100) {
                    gameWon = true;
                    Log.getLogger().info("The winner is " + player.getName() + "!");
                    break;
                }
            }
        }
    }

    /**
     * Processes the players turn.
     *
     * @param Player
     * @return Player
     */
    private static Player processTurn(Player player) {
        int moveForwardBy = player.spin();
        if ((player.getCurrentLocation() + moveForwardBy) <= 100) {
            // Get the sqaure at location (player.getCurrentLocation() + moveForwardBy)
            Square square = gameboard.getSquares().get((player.getCurrentLocation() + moveForwardBy - 1));
            if (square.getLocation() != square.getGoToLocation()) {
                String squareOrLadder = "LADDER";
                if (square.getGoToLocation() < square.getLocation()) {
                    squareOrLadder = "CHUTE";
                }
                player.setNextLocation(square.getGoToLocation());
                Log.getLogger().info(new StringBuilder().append(player.getName()).append(": ").append(player.getCurrentLocation()).append(" --> ").append(player.getCurrentLocation() + moveForwardBy).append(" --").append(squareOrLadder).append("--> ").append(player.getNextLocation()).toString());
            } else {
                player.setNextLocation(player.getCurrentLocation() + moveForwardBy);
                Log.getLogger().info(new StringBuilder().append(player.getName()).append(": ").append(player.getCurrentLocation()).append(" --> ").append(player.getNextLocation()).toString());
            }
            player.setCurrentLocation(player.getNextLocation());
        }
        try {
            Thread.sleep(750);
        } catch (InterruptedException ex) {
            Logger.getLogger(ChutesAndLadders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return player;
    }

    /**
     * Gets the command line options.
     *
     * @param args
     */
    private static boolean parseCommandLineOptions(String[] args) {
        boolean parseOK = true;
        CommandLineParser parser = new BasicParser();

        Options options = new Options();
        // help option
        options.addOption("help", false, "Prints usage.");
        // playersName option
        Option o = new Option("playerNames", true, "Names of (at least 2 and upto 4) players. E.g. \"Homer,Bart,Lisa,Marge\".");
        o.setArgs(4);//maximum 4 players
        o.setValueSeparator(',');// command separated player names
        options.addOption(o);
        // gameMode option
        options.addOption(new Option("gameMode", true, "Game mode: <1=Easy, 2=Default, 3=Hard>"));

        try {
            // parse the command line arguments
            CommandLine commandLine = parser.parse(options, args);
            if (commandLine.hasOption("help")) {
                usage(options);
            } else if (commandLine.hasOption("playerNames")) {
                if (commandLine.hasOption("gameMode")) {
                    gameMode = commandLine.getOptionValue("gameMode");
                    String[] names = commandLine.getOptionValues("playerNames");
                    if (names.length < 2 || names.length > 4) {
                        usage(options);
                        parseOK = false;
                    } else {
                        for (String playerName : names) {
                            players.add(new Player(playerName.trim()));
                        }
                        Log.getLogger().info("Players playing this game: " + Arrays.asList(names) + ", Game mode: " + gameMode);
                    }
                } else {
                    Log.getLogger().info("Option -gameMode is missing.");
                    usage(options);
                    parseOK = false;
                }
            } else {
                usage(options);
            }
        } catch (ParseException e) {
            parseOK = false;
            Log.getLogger().error("Exception while parsing the command line arguments: ", e);
            usage(options);
        }

        return parseOK;

    }

    /**
     * Prints the usage of the application.
     *
     * @param options
     */
    private static void usage(Options options) {
        (new HelpFormatter()).printHelp("ChutesAndLadders", options, true);
    }

}
