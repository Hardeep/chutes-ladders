package com.hmehta.model;

/**
 * Represents a Square on the board.
 * 
 * @author HMehta
 */
public class Square {
    private int location = 0;
    private int goToLocation = 0;
    
    public Square(int location, int goToLocation) {
        this.location = location;
        this.goToLocation = goToLocation;
    }
    
    /**
     * @return the location
     */
    public int getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(int location) {
        this.location = location;
    }

    /**
     * @return the goToLocation
     */
    public int getGoToLocation() {
        return goToLocation;
    }

    /**
     * @param goToLocation the goToLocation to set
     */
    public void setGoToLocation(int goToLocation) {
        this.goToLocation = goToLocation;
    }
    
    @Override
    public String toString() {
        return new StringBuilder().append("Square { location=").append(location).append(", goToLocation=").append(goToLocation).append(" }").toString();
    }
    
}
