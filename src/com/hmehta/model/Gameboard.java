package com.hmehta.model;

import java.util.List;
import com.hmehta.util.Log;
import java.util.ArrayList;
import com.hmehta.util.Constants;

/**
 * Represents the Gameboard
 *
 * @author HMehta
 */
public class Gameboard {

    private final int boardSize = 100;
    private int[] specialSquareLocation = null;
    private int[] specialSquareGoToLocation = null;
    private final List<Square> squares = new ArrayList();

    public Gameboard(String gameMode) {
        init(gameMode);
        Log.getLogger().debug("Game initialized...");
    }

    /**
     * Initializes the Gameboard
     */
    private void init(String gameMode) {

        if (gameMode.equalsIgnoreCase(Constants.GAME_MODE_DEFAULT)) {
            specialSquareLocation = Constants.DEFAULT_SPECIAL_SQUARE_LOCATION;
            specialSquareGoToLocation = Constants.DEFAULT_SPECIAL_SQUARE_GOTO_LOCATION;
        } else if (gameMode.equalsIgnoreCase(Constants.GAME_MODE_EASY)) {
            specialSquareLocation = Constants.EASY_SPECIAL_SQUARE_LOCATION;
            specialSquareGoToLocation = Constants.EASY_SPECIAL_SQUARE_GOTO_LOCATION;
        } else if (gameMode.equalsIgnoreCase(Constants.GAME_MODE_HARD)) {
            specialSquareLocation = Constants.HARD_SPECIAL_SQUARE_LOCATION;
            specialSquareGoToLocation = Constants.HARD_SPECIAL_SQUARE_GOTO_LOCATION;
        }

        for (int i = 0; i < boardSize; ++i) {
            squares.add(new Square(i + 1, i + 1));
        }
        for (int i = 0; i < specialSquareLocation.length; ++i) {
            squares.get(specialSquareLocation[i]).setGoToLocation(specialSquareGoToLocation[i]);

        }
        Log.getLogger().debug(squares.toString());
    }

    /**
     * @return the squares
     */
    public List<Square> getSquares() {
        return squares;
    }

    /**
     * @return the specialSquareLocation
     */
    public int[] getSpecialSquareLocation() {
        return specialSquareLocation;
    }

    /**
     * @return the specialSquareGoToLocation
     */
    public int[] getSpecialSquareGoToLocation() {
        return specialSquareGoToLocation;
    }

}
