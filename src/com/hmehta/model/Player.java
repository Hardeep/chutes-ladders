package com.hmehta.model;

/**
 * Represents the Player.
 *
 * @author HMehta
 */
public class Player {

    private String name = null;
    private int currentLocation = 0;
    private int nextLocation = 0;

    public Player(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the currentLocation
     */
    public int getCurrentLocation() {
        return currentLocation;
    }

    /**
     * @param currentLocation the currentLocation to set
     */
    public void setCurrentLocation(int currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     * @return the nextLocation
     */
    public int getNextLocation() {
        return nextLocation;
    }

    /**
     * @param nextLocation the nextLocation to set
     */
    public void setNextLocation(int nextLocation) {
        this.nextLocation = nextLocation;
    }

    /**
     * Spin the spinner (# b/w 1 & 6)
     * 
     * @return int
     */
    public int spin() {        
        return Spinner.spin();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Player {name=").append(getName()).append(", currentLocation=").append(currentLocation).append("}").toString();
    }

}
