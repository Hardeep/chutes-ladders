package com.hmehta.model;

import java.util.Random;

/**
 * Represents the Spinner.
 *
 * @author HMehta
 */
public class Spinner {

    private static final int MIN_COUNT = 1;
    private static final int MAX_COUNT = 6;
    
    /**
     * Simulates a spin (# b/w 1 & 6)
     * 
     * @return int
     */
    public static int spin() {
        return (new Random()).nextInt(MAX_COUNT - MIN_COUNT + 1) + MIN_COUNT;
    }
    
}
